package com.c0d3in3.finalproject.tools

interface ImageUploadCallback {
    fun onFinish(downloadUrl: String)
}