package com.c0d3in3.finalproject

object Constants {


    const val POST_TYPE_TEXT = 0
    const val POST_TYPE_IMAGE = 1
    const val POST_TYPE_VIDEO = 2

    const val RC_SIGN_IN = 9001

    const val OPEN_DETAILED_POST = 8001

    const val VIEW_TYPE_WALL_ITEM = 0
    const val VIEW_TYPE_LOADING = 1

    const val NOTIFICATION_START_FOLLOW = 0
    const val NOTIFICATION_LIKE_POST = 1
    const val NOTIFICATION_COMMENT = 2
    const val NOTIFICATION_LIKE_COMMENT = 3
}